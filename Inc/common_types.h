


#ifndef COMMON_TYPES
#define COMMON_TYPES

#include "stdint.h"


/* Global typedef -----------------------------------------------------------*/
typedef int32_t  s32;
typedef int16_t  s16;
typedef int8_t   s8;

typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t  u8;



typedef signed short fix4;
#define multfix4(a,b)  ((fix4)(((( signed long)(a))*(( signed long)(b)))>>4)) //multiply two fixed 7:4
#define float2fix4(a)  ((fix4)((a)*16.0)) // 2^4
#define fix2float4(a)  ((float)(a)/16.0)
#define fix2int4(a)    ((short)((a)>>4))
#define int2fix4(a)    ((fix4)((a)<<4))
#define divfix4(a,b)   ((fix4)((((signed long)(a)<<4)/(b))))
#define sqrtfix4(a)    (float2fix4(sqrt(fix2float4(a))))
#define absfix4(a)      ((a & 0x8000) ? (~a + 1) : a)






#endif
