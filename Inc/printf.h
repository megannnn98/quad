

#ifndef xPRINTF
#define xPRINTF

#include "string.h"


#if DEBUG
#include "stm32f0xx_hal.h"
#define PRINT(...)                                                            \
    do {                                                                      \
        extern char buffer[];                                                 \
        extern UART_HandleTypeDef H_UART;                                     \
        sprintf(buffer, __VA_ARGS__);                                         \
        HAL_UART_Transmit(&H_UART, (uint8_t*)buffer, strlen(buffer), 0x1000); \
        while(USART_ISR_TC != (H_UART.Instance->ISR & USART_ISR_TC));  \
    } while (0);
#else
//#define PRINT(...)
#endif

#endif