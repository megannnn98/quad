

#include "stm32f0xx_hal.h"
#include "MPU6050.h"

#define WRITE 0
#define READ 1




extern I2C_HandleTypeDef hi2c1;

static u8 MPU6050_resetBits(u8 regAddr, u8 val);
static u8 MPU6050_setBits(u8 regAddr, u8 val);
static uint8_t MPU6050_I2C_ByteRead(uint8_t regaddr);
static void MPU6050_I2C_ByteWrite(u8 writeAddr, u8 data);
uint8_t MPU6050_GetFullScaleAccelRange();
void MPU6050_SetFullScaleAccelRange(uint8_t range);
void MPU6050_SetClockSource(uint8_t source);
uint8_t MPU6050_GetDeviceID();
uint8_t MPU6050_GetIntStatus(void);
uint8_t MPU6050_TestConnection();
void MPU6050_StartCycleMode(u8 wakeCtrl);



void MPU6050_StartCycleMode(u8 wakeCtrl)
{
    mpu6050.setBits(MPU6050_RA_PWR_MGMT_2, (wakeCtrl & 0x03) << 6);
    mpu6050.setBits(MPU6050_RA_PWR_MGMT_1, 1 << 5);
}

/** Verify the I2C connection.
 * Make sure the device is connected and responds as expected.
 * @return True if connection is valid, FALSE otherwise
 */
uint8_t MPU6050_TestConnection()
{
    if (0x68 == MPU6050_GetDeviceID()) //0b110100; 8-bit representation in hex = 0x34
        return TRUE;
    else
        return FALSE;
}

static uint8_t MPU6050_GetIntStatus(void)
{
    uint8_t tmp = 0;
    tmp = mpu6050.readByte(MPU6050_RA_INT_STATUS);
    return tmp;
}

/** Get Device ID.
 * This register is used to verify the identity of the device (0b110100).
 * @return Device ID (should be 0x68, 104 dec, 150 oct)
 * @see MPU6050_RA_WHO_AM_I
 * @see MPU6050_WHO_AM_I_BIT
 * @see MPU6050_WHO_AM_I_LENGTH
 */
static uint8_t MPU6050_GetDeviceID()
{
    uint8_t itmp;
    itmp = mpu6050.readByte(MPU6050_RA_WHO_AM_I);
    return itmp;
}
/** Set clock source setting.
 * An internal 8MHz oscillator, gyroscope based clock, or external sources can
 * be selected as the MPU-60X0 clock source. When the internal 8 MHz oscillator
 * or an external source is chosen as the clock source, the MPU-60X0 can operate
 * in low power modes with the gyroscopes disabled.
 *
 * Upon power up, the MPU-60X0 clock source defaults to the internal oscillator.
 * However, it is highly recommended that the device be configured to use one of
 * the gyroscopes (or an external clock source) as the clock reference for
 * improved stability. The clock source can be selected according to the following table:
 *
 * <pre>
 * CLK_SEL | Clock Source
 * --------+--------------------------------------
 * 0       | Internal oscillator
 * 1       | PLL with X Gyro reference
 * 2       | PLL with Y Gyro reference
 * 3       | PLL with Z Gyro reference
 * 4       | PLL with external 32.768kHz reference
 * 5       | PLL with external 19.2MHz reference
 * 6       | Reserved
 * 7       | Stops the clock and keeps the timing generator in reset
 * </pre>
 *
 * @param source New clock source setting
 * @see MPU6050_GetClockSource()
 * @see MPU6050_RA_PWR_MGMT_1
 * @see MPU6050_PWR1_CLKSEL_BIT
 * @see MPU6050_PWR1_CLKSEL_LENGTH
 */
static void MPU6050_SetClockSource(uint8_t source)
{
    mpu6050.writeByte(MPU6050_RA_PWR_MGMT_1, source & 0x07);
}

/** Get full-scale accelerometer range.
 * The FS_SEL parameter allows setting the full-scale range of the accelerometer
 * sensors, as described in the table below.
 *
 * <pre>
 * 0 = +/- 2g
 * 1 = +/- 4g
 * 2 = +/- 8g
 * 3 = +/- 16g
 * </pre>
 *
 * @return Current full-scale accelerometer range setting
 * @see MPU6050_ACCEL_FS_2
 * @see MPU6050_RA_ACCEL_CONFIG
 * @see MPU6050_ACONFIG_AFS_SEL_BIT
 * @see MPU6050_ACONFIG_AFS_SEL_LENGTH
 */
static uint8_t MPU6050_GetFullScaleAccelRange()
{
    uint8_t itmp;
    itmp = mpu6050.readByte(MPU6050_RA_ACCEL_CONFIG) >> 3;
    return itmp;
}

/** Set full-scale accelerometer range.
 * @param range New full-scale accelerometer range setting
 * @see MPU6050_GetFullScaleAccelRange()
 */
static void MPU6050_SetFullScaleAccelRange(uint8_t range)
{
    mpu6050.setBits(MPU6050_RA_ACCEL_CONFIG, ((range & 0x03) << 3));
}

static void MPU6050_SoftReset()
{
    MPU6050_setBits(MPU6050_RA_PWR_MGMT_1, MPU6050_PWR1_DEVICE_RESET_BIT);
    while (MPU6050_PWR1_DEVICE_RESET_BIT == MPU6050_I2C_ByteRead(MPU6050_RA_PWR_MGMT_1))
        ;
}

/**
* @brief  Initializes the I2C peripheral used to drive the MPU6050
* @param  None
* @return None
*/
static void MPU6050_I2C_Init()
{/*
    GPIO_Init(MPU6050_SDA_GPIO, MPU6050_SDA_PIN, GPIO_Mode_Out_OD_HiZ_Fast);
    GPIO_Init(MPU6050_SCL_GPIO, MPU6050_SCL_PIN, GPIO_Mode_Out_OD_HiZ_Fast);

    CLK_PeripheralClockConfig(MPU6050_CLK_Peripheral_I2C1, ENABLE);
    I2C_Init(MPU6050_I2C, MPU6050_Speed, 12, I2C_Mode_I2C,
        I2C_DutyCycle_2, I2C_Ack_Enable, I2C_AcknowledgedAddress_7bit);
    I2C_Cmd(MPU6050_I2C, ENABLE);*/
  
    
}
static u8 MPU6050_setBits(u8 regAddr, u8 val)
{
    u8 itmp;
    itmp = MPU6050_I2C_ByteRead(regAddr);
    itmp = itmp | val;
    MPU6050_I2C_ByteWrite(regAddr, itmp);

    return MPU6050_I2C_ByteRead(regAddr);
}

static u8 MPU6050_resetBits(u8 regAddr, u8 val)
{
    u8 itmp;
    itmp = MPU6050_I2C_ByteRead(regAddr);
    itmp = itmp & ~val;
    MPU6050_I2C_ByteWrite(regAddr, itmp);

    return MPU6050_I2C_ByteRead(regAddr);
}





static void MPU6050_I2C_ByteWrite(u8 addr_write, u8 data)
{
    uint16_t dev_adr;

    dev_adr = (MPU6050_DEFAULT_ADDRESS | READ);
    data = addr_write;

     HAL_I2C_Master_Transmit(&hi2c1,dev_adr,(uint8_t*)data,1,200);

    dev_adr = (MPU6050_DEFAULT_ADDRESS);
    HAL_I2C_Master_Receive(&hi2c1,dev_adr,(uint8_t*)data,1,200);
    
    return;

}
static uint8_t MPU6050_I2C_ByteRead(uint8_t addr_read)
{
    uint8_t data[1];
    uint16_t dev_adr;

    dev_adr = (MPU6050_DEFAULT_ADDRESS | WRITE);
    data[0] = addr_read;

    HAL_I2C_Master_Transmit(&hi2c1,dev_adr,(uint8_t*)data,1,200);

    dev_adr = (MPU6050_DEFAULT_ADDRESS);
    HAL_I2C_Master_Receive(&hi2c1,dev_adr,(uint8_t*)data,1,200);
    
    return data[0];
    
}

/**
  * @brief  Структура дравера Mpu6050.
*/
const struct Mpu6050_driver mpu6050 = {
    .init_i2c = MPU6050_I2C_Init,
    .softReset = MPU6050_SoftReset,
    .writeByte = MPU6050_I2C_ByteWrite,
    .readByte = MPU6050_I2C_ByteRead,
    .setBits = MPU6050_setBits,
    .resetBits = MPU6050_resetBits,
    .getFullScaleAccelRange = MPU6050_GetFullScaleAccelRange,
    .setFullScaleAccelRange = MPU6050_SetFullScaleAccelRange,
    .setClockSource = MPU6050_SetClockSource,
    .getDeviceID = MPU6050_GetDeviceID,
    .getIntStatus = MPU6050_GetIntStatus,
    .testConnection = MPU6050_TestConnection,
    //	.startCycleMode = MPU6050_StartCycleMode,
};

/**
 * @}
 */ /* end of group MPU6050_Library */
